
'use strict';

const ChildProcess = require('child_process');
const Logger = require('./log/logger');
const PluginLock = require('./plugin-lock');
const GuidGenerator = require('z-abs-corelayer-cs/guid-generator');
const ResponseResultSuccess = require('./response-result').ResponseResultSuccess;
const ResponseResultFailure = require('./response-result').ResponseResultFailure;
const ResponseResultError = require('./response-result').ResponseResultError;


class PluginBase {
  constructor(restType) {
    this.dataName = this.constructor.name;
    this.restType = restType;
    this.request = null;
    this.cb = null;
    this.cbMessage = null;
    this.dataResponse = null;
  }
  
  readLock(name, cb) {
    PluginLock.readLock(name, cb);
  }
  
  writeLock(name, cb) {
    PluginLock.writeLock(name, cb);
  }
  
  responsePartSuccess(data) {
    this.cb({
      name: this.dataName,
      index: this.request.index,
      result: new ResponseResultSuccess(), 
      data: data
    });
  }

  responsePartFailure(msg, status) {
    this.cb({
      name: this.dataName,
      index: this.request.index,
      result: new ResponseResultFailure(msg, status, this.restType)
    });
  }
  
  responsePartError(msg, err) {
    this.cb({
      name: this.dataName,
      index: this.request.index,
      result: new ResponseResultError(msg, err)
    });
  }
  
  forkChildWorkerSlaveProcess(name, pluginPath, execArgv, done) {
    this.dataResponse.forkChildWorkerSlaveProcess(name, this.request.sessionId, pluginPath, execArgv, (pid) => {
      try {
        if(undefined !== done) {
          done(pid);
        }
      }
      catch(err) {
        LOG_ERROR(Logger.ERROR.CATCH, 'Exception in forkChildWorkerSlaveProcess callback: ', err, err.stack);
      }
    });
  }
  
  killChildWorkerSlaveProcess(done) {
    this.dataResponse.killChildWorkerSlaveProcess(this.request.sessionId, (code) => {
      try {
        if(undefined !== done) {
          done(code);
        }
      }
      catch(err) {
        LOG_ERROR(Logger.ERROR.CATCH, 'Exception in killChildWorkerSlaveProcess callback: ', err, err.stack);
      }
    });
  }
  
  sendRequestToChildWorkerSlaveProcess(request, cb) {
    const sessionData = this.dataResponse.getSession(this.request.sessionId);
    this.dataResponse.sendRequestToChildWorkerSlaveProcess(sessionData, GuidGenerator.create(), request, cb, this.cbMessage);
  }
  
  forkChildWorkerProcess(name, pluginPaths, execArgv, done) {
    const requests = new Map();
    let childProcess = null;
    if(Array.isArray(pluginPaths)) {
      childProcess = ChildProcess.fork(`${name}.js`, ['./dist/server', pluginPaths.length, ...pluginPaths, ...execArgv], {execArgv: []});
    }
    else {
      childProcess = ChildProcess.fork(`${name}.js`, ['./dist/server', 1, pluginPaths, ...execArgv], {execArgv: []});
    }
    childProcess.on('message', (msg) => {
      if('message' === msg.type) {
        this.handleMessageFromWorker(msg.msg, (answerMsg) => {
          const msgAnswerEnvelope = {
          id: msg.msg.id,
            type: 'message',
            msg: answerMsg
          };
          childProcess.send(msgAnswerEnvelope);
        });
      }
      else if('response' === msg.type) {
        const cbResponse = requests.get(msg.msg.id);
        requests.delete(msg.msg.id);
        cbResponse(msg.msg);
        childProcess.kill();
      }
      else if('init' === msg.type) {
        done(childProcess.pid);
      }
    });
    childProcess.on('exit', (code) => {});
    return {
      process: childProcess,
      sendRequest: (requestName, sessionId, ...parameters) => {
        const params = [...parameters];
        const id = GuidGenerator.create();
        if(1 <= parameters.length && typeof parameters[parameters.length - 1] === 'function') {
          requests.set(id, parameters[parameters.length - 1]);
          params.pop();
        }
        const msg = {
          id: id,
          type: 'request',
          msg: {
            id: id,
            requests: [{
              name: requestName,
              sessionId: sessionId,
              params: params
            }]
          }
        };
        LOG_ENGINE(Logger.ENGINE.MSG, `REQ[ipc] => : ${JSON.stringify(msg)}`);
        childProcess.send(msg);
      }/*,
      send(msg) {
        const msg = {
        id: id,
          type: 'message',
          msg: msg
        };
        childProcess.send(msg);
      }*/
    };
  }
  
  setSessionData(name, data) {
    this.dataResponse.setSessionData(this.request.sessionId, name, data);
    return data;
  }
  
  getSessionData(name) {
    return this.dataResponse.getSessionData(this.request.sessionId, name);
  }
  
  clearSession(done) {
    this.dataResponse.clearSession(this.request.sessionId, done);
  }

  sendMessage(msg) {
    this.cbMessage(msg);
  }

  handleRequest(request, cb, cbMessage, dataResponse) {
    this.request = request;
    this.cb = cb;
    this.cbMessage = cbMessage;
    this.dataResponse = dataResponse;
    try {
      if(!this.onRequest) {
        return console.log(`${this.constructor.name}.onRequest is not implemented.`);
      }
      this.onRequest(...request.params);
    }
    catch(err) {
      console.log('Exception in handleRequest.');
      LOG_ERROR(Logger.ERROR.CATCH, `Error in ${this.dataName}.onRequest()`, err, err.stack);
      return this.responsePartError(`${this.dataName}.onRequest exception: ${err}`);
    }
  }
  
  handleMessageFromClient(msg) {
    if(undefined !== this.onMessageFromClient && typeof this.onMessageFromClient === 'function') {
      try {
        this.onMessageFromClient(msg);
      }
      catch(err) {
        LOG_ERROR(Logger.ERROR.CATCH, `Error in ${this.dataName}.onMessageFromClient()`, err, err.stack);
      }
      return true;
    }
    return false;
  }
  
  handleMessageToClient(msg, cbMessage) {
    if(undefined !== this.onMessageToClient && typeof this.onMessageToClient === 'function') {
      try {
        this.onMessageToClient(msg, cbMessage);
      }
      catch(err) {
        LOG_ERROR(Logger.ERROR.CATCH, `Error in ${this.dataName}.onMessageToClient()`, err, err.stack);
      }
      return true;
    }
    return false;
  }
  
  handleMessageFromWorker(msg, cbMessage) {
    if(undefined !== this.onMessageFromWorker && typeof this.onMessageFromWorker === 'function') {
      try {
        this.onMessageFromWorker(msg, cbMessage);
      }
      catch(err) {
        LOG_ERROR(Logger.ERROR.CATCH, `Error in ${this.dataName}.onMessageFromWorker()`, err, err.stack);
      }
      return true;
    }
    else {
      return false;
    }
  }

  handleMessageFromParent(msg, cbMessage) {
    if(undefined !== this.onMessageFromParent && typeof this.onMessageFromParent === 'function') {
      try {
        this.onMessageFromParent(msg, cbMessage);
      }
      catch(err) {
        LOG_ERROR(Logger.ERROR.CATCH, `Error in ${this.dataName}.onMessageFromParent()`, err, err.stack);
      }
      return true;
    }
    else {
      return false;
    }
  }
  
  handleClose(done) {
    if(undefined !== this.onClose && typeof this.onClose === 'function') {
      try {
        this.onClose((e) => {
          done(e);
        });
      }
      catch(err) {
        LOG_ERROR(Logger.ERROR.CATCH, `Error in ${this.dataName}.onClose()`, err, err.stack);
        done(err);
      }
    }
    else {
      done();
    }
  }
}


PluginBase.ADD = 'Add';
PluginBase.DELETE = 'Delete';
PluginBase.GET = 'Get';
PluginBase.UPDATE = 'Update';


module.exports = PluginBase;
