
'use strict';


class Responses {
  constructor(id) {
    this.id = id;
    this.responses = [];
  }
  
  add(response) {
    this.responses.push(response);
  }
}

class ResponseQueue {
  constructor(cb, externalCb, id) {
    this.instanceId = ++ResponseQueue.id;
    this.calls = 0;
    this.cb = cb;
    this.externalCb = externalCb;
    this.id = id;
    this.responses = new Responses(id);
  }
  
  add() {
    ++this.calls;
  }
  
  remove() {
    if(0 == --this.calls) {
      this.cb(this.responses, this.externalCb);
    }
  }
  
  done(response) {
    this.responses.add(response);
    if(0 === --this.calls) {
      this.cb(this.responses, this.externalCb);
    }
  }
}

ResponseQueue.id = 0;

module.exports = ResponseQueue;
