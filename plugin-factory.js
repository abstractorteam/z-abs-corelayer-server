
'use strict';

const ActorPathDist = require('./path/actor-path-dist');
const Logger = require('./log/logger');
const Fs = require('fs');
const Path = require('path');


class PluginFactory {
  constructor() {
    this.prefix = 'plugin_data_';
    this.prefix_length = this.prefix.length;
    this.paths = [];
    this.dataPlugins = new Map();
  }

  create(name) {
    let foundFactoryPath = this.dataPlugins.get(name);
    if(undefined !== foundFactoryPath) {
      //console.error('FACTORY PATH: ' + foundFactoryPath);
      let f = require(foundFactoryPath);
      return new f();
    }
    console.error('FACTORY ERROR: ' + name);
  }
  
  setPaths(paths) {
    if(Array.isArray(paths)) {
      paths.forEach((path) => {
        this.paths.push(Path.join(ActorPathDist.getLayersPath(), path));
      });
    }
    else {
      this.paths.push(Path.join(ActorPathDist.getLayersPath(), paths));
    }
  }
  
  load(done, path = ActorPathDist.getActorDistServerPluginDataPath()) {
    const loadPath = this.paths.concat(path);
    this.dataPlugins = new Map();
    let pendings = loadPath.length;
    loadPath.forEach((p) => {
      this.loadPlugins(p, () => {
        if(0 === --pendings) {
          done();
        }
      }); 
    });
  }
  
  loadPlugins(path, done) {
    Fs.lstat(path, (err, stat) => {
      if(err) {
        return done();
      }
      if(stat.isDirectory()) {
        // We have a directory: do a tree walk
        Fs.readdir(path, (err, files) => {
          let file, length = files.length;
          let pendings = length;
          if(0 === pendings) {
            done();
          }
          else {
            for(let i = 0; i < length; ++i) {
              file = Path.join(path, files[i]);
              this.loadPlugins(file, () => {
                if(0 === --pendings) {
                  done();
                }
              });
            }
          }
        });
      }
      else if(stat.isFile()) {
        let splitFilePath = path.split(Path.sep);
        let filePath = splitFilePath[splitFilePath.length - 2];
        let fileName = splitFilePath[splitFilePath.length - 1];
        let names = fileName.split('.');
        if(names[0].startsWith(this.prefix)) {
          let name = names[0].substring(this.prefix_length);
          let factoryPath = path.substring(0, path.lastIndexOf(Path.sep)) + Path.sep + names[0];
          try {
            this.dataPlugins.set(name, factoryPath);
            LOG_ENGINE(Logger.ENGINE.DP, `Plugin name: '${name}', path: '${factoryPath}'`);
          }
          catch(err) {
            LOG_ERROR(Logger.ERROR.CATCH, `Plugin name: '${name}', path: '${factoryPath}'.`, err);
          }
        }
        done();
      }
    });
  }
}

module.exports = new PluginFactory();
