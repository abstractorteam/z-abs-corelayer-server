
'use strict';

const Logger = require('./log/logger');
const ResponseQueue = require('./response-queue');
let PluginFactory = require('./plugin-factory');
const ActorPathDist = require('./path/actor-path-dist');
const ChildProcess = require('child_process');
const Path = require('path');


class DataResponse {
  constructor() {
    this.wsServers = [];
    this.sessionDataCache = new Map();
  }
  
  initMainServer(pluginFactoryPaths, wsServers, done) {
    this.wsServers = wsServers;
    PluginFactory.setPaths(pluginFactoryPaths);
    PluginFactory.load(() => {
      LOG_ENGINE(Logger.ENGINE.MSG, `LOADED 1`);
      done();
    });
  }
  
  _safeJsonParse(string) {
    try {
      return JSON.parse(string);
    }
    catch(err) {
      return null;
    }
  }
  
  handleHttpRequest(httpReq, cb) {
    let rawBody = '';
    httpReq.on('data', (chunk) => {
      rawBody += chunk;
    });
    httpReq.on('end', () => {
      LOG_ENGINE(Logger.ENGINE.MSG, `REQ[http] >= : ${rawBody}`);
      const requestObject = this._safeJsonParse(rawBody);
      if(null !== requestObject) {
        this._handleRequests(requestObject, (result) => {
          LOG_ENGINE(Logger.ENGINE.MSG, `RES[http] => : ${result}`);
          cb(JSON.stringify(result));
        });
      }
      else {
        cb(null);
      }
    });
  }
  
  handleWsConnection(ws) {
    const webSocketData = new Map();
    ws.on('message', (message) => {
      process.nextTick(() => {
        const wsMessage = JSON.parse(message);
        if('DP_REQ' === wsMessage.type) {
          LOG_ENGINE(Logger.ENGINE.MSG, `REQ[ws] >= : ${message}`);
          this._handleRequests(wsMessage.data, (result) => {
            process.nextTick((result) => {
              ws.send(JSON.stringify(result));
              LOG_ENGINE(Logger.ENGINE.MSG, `RES[ws] => : ${result}`);
            }, result);
          }, (msg, sessionId) => {
            const sessionData = this.getSession(sessionId);
            if(undefined !== sessionData) {
              let proxy = false;
              sessionData.dataPluginsInternal.forEach((dataPlugin) => {
                if(dataPlugin.handleMessageToClient(msg, (message) => {
                  this._sendMessageToClient(ws, message, sessionId);
                })) {
                  proxy = true;
                }
              });
              if(!proxy) {
                this._sendMessageToClient(ws, msg, sessionId);
              }
            }
            else {
              LOG_ENGINE(Logger.ENGINE.MSG, `MSG[ws] => DROPPED, no session: ${JSON.stringify(msg)}`);
            }
          }, webSocketData);
        }
        else if('DP_MSG' === wsMessage.type) {
          const sessionData = this.getSession(wsMessage.sessionId);
          if(undefined !== sessionData) {
            LOG_ENGINE(Logger.ENGINE.MSG, `MSG[ws] >= : ${JSON.stringify(wsMessage, null, 2)}`);
            let proxy = false;
            sessionData.dataPluginsInternal.forEach((dataPlugin) => {
              if(dataPlugin.handleMessageFromClient(wsMessage.data)) {
                proxy = false;
              }
            });
            if(!proxy) {
              //this._sendMessageToClient(ws, msg);
            }
          }
          else {
            LOG_ENGINE(Logger.ENGINE.MSG, `MSG[ws] >= DROPPED, no session: ${JSON.stringify(wsMessage)}`);
          }
          //webSocketData.get
          //ws.dataPlugin.handleMessage(wsMessage.data);
        }
      });
    });
    ws.on('close', () => {
      process.nextTick(() => {
        webSocketData.forEach((sessionData) => {
          sessionData.dataPluginsInternal.forEach((dataPlugin) => {
            let pendings = sessionData.dataPluginsInternal.length;
            dataPlugin.handleClose((err) => {
              process.nextTick(() => {
                if(0 === --pendings) {
                  this.clearSession(sessionData.sessionId, (code) => {
                    LOG_ENGINE(Logger.ENGINE.MSG, `CLOSE sessionId: ${sessionData.sessionId} code = ${code}`);
                  });
                }
              });
            });
          });
        });
      });
    });
  }
  
  forkChildWorkerSlaveProcess(name, sessionId, pluginPaths, execArgv, done) {
    let childProcess = null;
    if(Array.isArray(pluginPaths)) {
      childProcess = ChildProcess.fork(`${name}.js`, [Reflect.get(global, 'dynamicConfigStringified@abstractor'), ActorPathDist.getActorDistServerPluginDataPath(), pluginPaths.length, ...pluginPaths, ...execArgv], {execArgv: [], silent: false});
    }
    else {
      childProcess = ChildProcess.fork(`${name}.js`, [Reflect.get(global, 'dynamicConfigStringified@abstractor'), 1, pluginPaths, ...execArgv], {execArgv: [], silent: false});
    }
    const sessionData = this.getSession(sessionId);
    sessionData.process = childProcess;
    pluginPaths.forEach((pluginPath) => {
      sessionData.dataPluginsExternal.push(Path.parse(pluginPath).base);
    });
    childProcess.on('message', (msg) => {
      if('message' === msg.type) {
        const cbs = sessionData.requests.get(msg.id);
        LOG_ENGINE(Logger.ENGINE.MSG, `MSG[ipc] >= : ${JSON.stringify(msg)}`);
        let proxy = false;
        sessionData.dataPluginsInternal.forEach((dataPlugin) => {
          proxy = dataPlugin.handleMessageFromWorker(msg.msg, cbs.cbMessage);
        });
        if(!proxy) {
          cbs.cbMessage(msg.msg);
        }
      }
      else if('response' === msg.type) {
        const cbs = sessionData.requests.get(msg.id);
        LOG_ENGINE(Logger.ENGINE.MSG, `RES[ipc] >= : ${JSON.stringify(msg)}`);
        cbs.cb(msg.msg);
        cbs.cb = null;
      }
      else if('init' === msg.type) {
        sessionData.processStatus = DataResponse.PROCESS_STATUS_RUNNING;
        done(childProcess.pid);
      }
    });
    childProcess.on('exit', (code) => {
      sessionData.process = undefined;
      sessionData.processStatus = DataResponse.PROCESS_STATUS_NOT_RUNNING;
    });
  }
  
  killChildWorkerSlaveProcess(sessionId, done) {
    const sessionData = this.getSession(sessionId);
    if(undefined !== sessionData && DataResponse.PROCESS_STATUS_RUNNING === sessionData.processStatus) {  
      sessionData.process.on('exit', (code) => {
        sessionData.process = undefined;
        sessionData.processStatus = DataResponse.PROCESS_STATUS_NOT_RUNNING;
        done(code);
      });
      sessionData.processStatus = DataResponse.PROCESS_STATUS_CLOSING;
      sessionData.process.kill();
    }
    else {
      done(-1);
    }
  }
  
  createSessionData(sessionId, webSocketData) {
    const sessionData = {
      sessionId: sessionId,
      process: undefined,
      processStatus: DataResponse.PROCESS_STATUS_NOT_RUNNING,
      requests: new Map(),
      webSocketData: webSocketData,
      sessionData: new Map(),
      dataPluginsInternal: [],
      dataPluginsExternal: []
    };
    this.sessionDataCache.set(sessionId, sessionData);
    return sessionData;
  }

  setSessionData(sessionId, name, data) {
    let sessionData = this.getSession(sessionId);
    sessionData.sessionData.set(name, data);
    return data;
  }
  
  getSession(sessionId) {
    return this.sessionDataCache.get(sessionId);
  }
  
  getSessionData(sessionId, name) {
    let sessionData = this.getSession(sessionId);
    return undefined !== sessionData ? sessionData.sessionData.get(name) : undefined;
  }
  
  clearSession(sessionId, done) {
    let sessionData = this.getSession(sessionId);
    if(undefined !== sessionData) {
      this.killChildWorkerSlaveProcess(sessionId, (code) => {
        sessionData.webSocketData.delete(sessionId);
        this.sessionDataCache.delete(sessionId);
        done && done(code);
      });
    }
  }

  sendRequestToChildWorkerSlaveProcess(sessionData, id, request, cb, cbMessage) {
    sessionData.requests.set(id, {
      cb: cb,
      cbMessage: cbMessage
    });
    const msg = {
      id: id,
      type: 'request',
      msg: {
        id: id,
        requests: [request]
      }
    };
    LOG_ENGINE(Logger.ENGINE.MSG, `REQ[ipc] => : ${JSON.stringify(msg)}`);
    sessionData.process.send(msg);
  }
  
  initServerChildProcess(pluginPath) {
    PluginFactory.load(() => {
      process.on('message', (msg) => {
        if('message' === msg.type) {
          LOG_ENGINE(Logger.ENGINE.MSG, `MSG[ipc] >= : ${JSON.stringify(msg)}`);
        }
        else if('request' === msg.type) {
          LOG_ENGINE(Logger.ENGINE.MSG, `REQ[ipc] >= : ${JSON.stringify(msg)}`);
          this._handleRequests(msg.msg, (result) => {
            this._sendResponseToParentProcess(msg.id, result);
          }, (message) => {
            this._sendMessageToParentProcess(msg.id, message);
          });
        }
        else {
          // TODO: log
        }
      });
      process.send({
        type: 'init'
      });
      LOG_ENGINE(Logger.ENGINE.MSG, `LOADED 1`);
    }, pluginPath);
  }
  
  initServerWorkerProcess(pluginPath) {
    PluginFactory.load(() => {
      process.on('message', (msg) => {
        if('message' === msg.type) {
          let sessionData = this.getSession(msg.id);
          sessionData.dataPluginsInternal.forEach((dataPlugin) => {
            dataPlugin.handleMessageFromParent(msg.msg);
          });
          LOG_ENGINE(Logger.ENGINE.MSG, `MSG[ipc] >= : ${JSON.stringify(msg)}`);
        }
        else if('request' === msg.type) {
          LOG_ENGINE(Logger.ENGINE.MSG, `REQ[ipc] >= : ${JSON.stringify(msg)}`);
          this._handleRequests(msg.msg, (result) => {
            this._sendResponseToParentProcess(msg.id, JSON.parse(result));
            this.getSession(msg.id);
          }, (message) => {
            this._sendMessageToParentProcess(msg.id, message);
          });
        }
        else {
          // TODO: log
        }
      });
      process.send({
        type: 'init'
      });
      LOG_ENGINE(Logger.ENGINE.MSG, `LOADED 1`);
    }, pluginPath);
  }
  
  _sendMessageToClient(ws, msg, sessionId) {
    process.nextTick((msg) => {
      ws.send(JSON.stringify(msg), (err) => {
        if(!err) {
          LOG_ENGINE(Logger.ENGINE.MSG, `MSG[ws] => : ${stingMsg}`);
        }
        else {
          LOG_ERROR(Logger.ERROR.ERR, `Websocket message from server to client failed.`, err);  
        }
      });
    }, msg);
  }

  _sendResponseToParentProcess(id, result) {
    process.nextTick(() => {
      const msg = {
        id: id,
        type: 'response',
        msg: result
      };
      LOG_ENGINE(Logger.ENGINE.MSG, `RES[ipc] => : ${JSON.stringify(msg)}`);
      process.send(msg);
    });
  }
  
  _sendMessageToParentProcess(id, message) {
    const f = () => {
      const msg = {
        id: id,
        type: 'message',
        msg: message
      };
      LOG_ENGINE(Logger.ENGINE.MSG, `MSG[ipc] => : ${JSON.stringify(msg)}`);
      process.send(msg);
    };
    if(!message.debug) {
      process.nextTick(f, message);
    }
    else {
      process.nextTick(() => {
        process.nextTick(f, message);
      }, f, message);
    }
  }
    
  _getDataPlugin(name) {
    const dataPlugin = PluginFactory.create(name);
    if(undefined === dataPlugin) {
      PluginFactory = require('./plugin-factory');
      PluginFactory.load(() => {
        LOG_ENGINE(Logger.ENGINE.MSG, `LOADED 2`);
      });
      return PluginFactory.create(name);  
    }
    return dataPlugin;
  }
  
  _handleRequestLocal(request, dataPlugin, queue, cbMessage) {
    try {
      LOG_ENGINE(Logger.ENGINE.MSG, `      ===> ${request.name}(${this._logParameters(request.params)})`);
      dataPlugin.handleRequest(request, (response) => {
        LOG_ENGINE(Logger.ENGINE.MSG, `      <=== ${request.name}`);
        queue.done(response);
      }, cbMessage, this);
    }
    catch(err) {
      LOG_ERROR(Logger.ERROR.CATCH, `Data Plugin exception: '${request.name}'. Message: ${err}`, err);
      queue.done({name: request.name, index: request.index, result: { code: 'error', msg: 'Data Plugin exception: ' + request.name }});
    }
  }

  _handleRequests(rawRequest, cbRequest, cbMessage, webSocketData) {
    const queue = new ResponseQueue((responses, cb) => {
      cb(responses);
    }, (result) => {
      cbRequest(result);
    }, rawRequest.id);
    rawRequest.requests.forEach((request) => {
      queue.add();
      const dataPlugin = this._getDataPlugin(request.name);
      if(undefined === dataPlugin) {
        LOG_ERROR(Logger.ERROR.ERR, `Unknown Data Plugin: ${request.name}.`, undefined);
        queue.done({name: request.name, index: request.index, result: { code: 'error', msg: 'Unknown Data Plugin: ' + request.name }});
      }
      else if(undefined === request.sessionId) {
        this._handleRequestLocal(request, dataPlugin, queue, (msg) => {
          cbMessage(msg);
        });
      }
      else {
        let sessionData = this.getSession(request.sessionId);
        if(undefined === sessionData)
        {
          sessionData = this.createSessionData(request.sessionId, webSocketData)
          if(undefined !== webSocketData) {
            webSocketData.set(request.sessionId, sessionData);
          }
        }
        const externalFound = sessionData.dataPluginsExternal.find((dataPlugin) => {
          return dataPlugin.dataName;
        });
        if(externalFound) {
          this.sendRequestToChildWorkerSlaveProcess(sessionData, dataPlugin.instanceId, request, (response) => {
            queue.done(response);
          }, (msg) => {
            cbMessage(msg, request.sessionId);
          });
        }
        else {
          sessionData.dataPluginsInternal.push(dataPlugin);
          this._handleRequestLocal(request, dataPlugin, queue, (msg) => {
            cbMessage(msg, request.sessionId);
          });
        }
      }    
    });
  }
  
  _logParameters(parameters) {
    let logParameters = parameters.map((parameter) => {
      if(typeof parameter === 'string') {
        return `'${parameter.replace(/\r/g, '\\r').replace(/\n/g, '\\n')}'`;
      }
      else {
        return `${parameter}`;
      }
    });
    return logParameters.join(', ');
  }
}

DataResponse.basePath = './dist/Layers/z-abs-corelayer-server';
DataResponse.PROCESS_STATUS_NOT_RUNNING = 0;
DataResponse.PROCESS_STATUS_RUNNING = 1;
DataResponse.PROCESS_STATUS_CLOSING = 2;


module.exports = new DataResponse();
