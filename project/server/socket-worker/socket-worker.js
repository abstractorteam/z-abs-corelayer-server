
'use strict';


const { Worker, isMainThread, parentPort } = require('worker_threads');
const ConnectionHandler = require('./connection-handler');


class SocketWorker {
  constructor(path, cbRunning) {
    this.path = path;
    this.cbRunning = cbRunning;
  }
  
  run(id, host, srcAddress, parentObject) {
    const runId = id;
    const worker = new Worker(__filename);
    worker.once('message', (message) => {
      this.cbRunning(message.errors, message.port);
      worker.on('message', (message) => {
        const func = Reflect.get(parentObject, `on${message.cmd}`).bind(parentObject);
        if(null !== func) {
          if(SocketWorker.MESSAGE === message.type) {
            func(runId, ...message.params);
          }
          else {
            func((err, ...params) => {
              worker.postMessage({
                cmd: message.cmd,
                id: message.id,
                data: {
                  err: err,
                  params: params
                }
              });
            }, ...message.data);
          }
        }
        else {// DO
          console.log(`Error function 'on${message.cmd}' not found.`);
        }
      });
    });
    worker.on('exit', (exitCode) => {
      //console.log(id, `worker.exit code:${exitCode}`);
    });
    worker.postMessage({
      path: this.path,
      id: id,
      host: host,
      srcAddress: srcAddress
    });
  }
}

if(!isMainThread) {
  let currentCbId = 0;
  const cbs = new Map();
  parentPort.once('message', (message) => {
    try {
      const Impl = require(message.path);
      const impl = new Impl(message.id);
      const connectionHandler = new ConnectionHandler(message.id, impl, (done, cmd, ...params) => {
        if(0 !== params.length) {
          const id = ++currentCbId;
          cbs.set(id, done);
          parentPort.postMessage({
            type: SocketWorker.METHOD,
            cmd: cmd,
            id: id,
            data: params
          });
          parentPort.once('message', (respone) => {
            const cbDone = cbs.get(respone.id);
            cbDone(respone.data.err, ...respone.data.params);
            cbs.delete(respone.id);
          });
        }
        else {
          done(new Error('Wrong format'));
        }
      }, (cmd, transferables, ...params) => {
        const msg = {
          type: SocketWorker.MESSAGE,
          cmd,
          params
        };
        parentPort.postMessage(msg, transferables);
      });
      connectionHandler.run(message.host, message.srcAddress, (errors, port) => {
        parentPort.postMessage({
          errors: errors,
          port: port
        });
      });
    }
    catch(err) {
      console.log(err);
      parentPort.postMessage({
        errors: [err],
        port: 0
      });
    }
  });
  parentPort.on('messageerror', (err) => {
    console.log('EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE', err);
  });
}

SocketWorker.METHOD = 0;
SocketWorker.MESSAGE = 1;


module.exports = SocketWorker;
