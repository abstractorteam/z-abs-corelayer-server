
'use strict';

const Net = require('net');


class ConnectionHandler {
  constructor(id, impl, cbMethod, cbMessage) {
    this.id = id;
    this.impl = impl;
    this.cbMethod = cbMethod;
    this.cbMessage = cbMessage;
    this.srcAddress = null;
    this.srcAddressResolved = ConnectionHandler.ADDRESS_NONE;
    this.serverSocket = null;
    this.incommingSocket = null;
    this.outgoingSocket = null;
  }
  
  _firstAndConnect(incommingSocket) {
    incommingSocket.once('data', (chunk) => {
      incommingSocket.pause();
      if(this.impl.onFirstIncommingData(chunk)) {
        incommingSocket.on('data', (chunk) => {
          this.impl.onIncommingData(chunk);
        });
      }
      else {
        this._firstAndConnect(incommingSocket);
        incommingSocket.resume();
      }
    });
  }
  
  run(host, srcAddress, done) {
    this.impl.setConnectionHandler(this);
    this.srcAddress = srcAddress;
    this.serverSocket = Net.createServer((incommingSocket) => {
      this.incommingSocket = incommingSocket;
      this.impl.setIncommingSocket(incommingSocket);
      this._firstAndConnect(incommingSocket);
      incommingSocket.on('end', () => {
        if(null !== this.outgoingSocket) {
          this.outgoingSocket.end();
        }
      });
      incommingSocket.on('close', () => {
        if(null !== this.outgoingSocket) {
          this.outgoingSocket.destroy();
        }
        this.incommingSocket = null;
        if(null === this.outgoingSocket) {
          this.serverSocket.close();
        }
      });
      incommingSocket.on('error', (err) => {
      });
    });
    this.serverSocket.listen({
      host: host,
      port: 0
    }, (err) => {
      if(err) {
      }
      done(err, this.serverSocket.address().port);
    });
  }
  
  resolveUrl(url, done) {
    this.cbMethod(done, 'DnsGet', url);
  }
  
  connect(dstAddress, done) {
    const address = {
      host: dstAddress.host,
      port: dstAddress.port,
      localAddress: this.srcAddress.host,
      localPort: this.srcAddress.port
    };
    let connecting = true;
    const outgoingSocket = Net.connect(address, (err) => {
      this.outgoingSocket = outgoingSocket;
      this.incommingSocket.resume();
      connecting = false;
      this.srcAddress.port = outgoingSocket.localPort;
      if(this.impl.onConnected) {
        this.impl.onConnected(err, this.srcAddress, dstAddress);
      }
      done(err, outgoingSocket);
    });
    outgoingSocket.on('data', (chunk) => {
      this.impl.onOutgoingData(chunk);
    });
    outgoingSocket.on('end', () => {
      if(null !== this.incommingSocket) {
        this.incommingSocket.end();
      }
      if(this.impl.onClosing) {
        this.impl.onClosing();
      }
    });
    outgoingSocket.on('close', () => {
      if(null !== this.incommingSocket) {
        this.incommingSocket.destroy();
      }
      this.outgoingSocket = null;
      if(this.impl.onClosed) {
        this.impl.onClosed();
      }
      if(null === this.incommingSocket) {
        this.serverSocket.close();
      }
    });
    outgoingSocket.on('error', (err) => {
      if(connecting) {
        done(err);
      }
    });
    if(this.impl.onConnecting) {
      this.impl.onConnecting(this.srcAddress, dstAddress);
    }
  }
  
  sendMessage(cmd, transferables, ...params) {
    this.cbMessage(cmd, transferables, ...params);
  }
}

ConnectionHandler.ADDRESS_NONE = 0;
ConnectionHandler.ADDRESS_OK = 1;
ConnectionHandler.ADDRESS_ERR = 2;

module.exports = ConnectionHandler;
