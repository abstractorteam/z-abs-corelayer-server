
'use strict';

const HighResolutionDate = require('z-abs-corelayer-cs/high-resolution-date');

// TODO: Optimize and Test !!!
class HighResolutionTimestamp {
  static getDate(timestamp) {
    if(undefined === timestamp) {
      timestamp = process.hrtime();
    }
    const milliSeconds = ((timestamp[0] - HighResolutionDate.timeStamp[0]) * 1000) + ((timestamp[1] - HighResolutionDate.timeStamp[1]) * 0.000001);
    return new Date(HighResolutionDate.dateStamp + milliSeconds);
  }
  
  static getHighResolutionDate(timestamp) {
    if(undefined === timestamp) {
      timestamp = process.hrtime();
    }
    const secondsDiff = timestamp[0] - HighResolutionDate.timeStamp[0] + ((timestamp[1] - HighResolutionDate.timeStamp[1]) * 0.000000001);
    const secondsDiffFloor = Math.floor(secondsDiff);
    const milliSecondsDecimal = 1000 * (secondsDiff - secondsDiffFloor);
    return new HighResolutionDate(new Date(HighResolutionDate.dateStamp + 1000 * secondsDiff), milliSecondsDecimal.toFixed(3));
  }
  
  static getTimestampMilliSeconds(timestamp) {
    if(undefined === timestamp) {
      timestamp = process.hrtime();
    }
    return (timestamp[0] - HighResolutionDate.timeStamp[0]) * 1000 + ((timestamp[1] - HighResolutionDate.timeStamp[1]) * 0.000001);
  }
  
  static getMilliseconds(hrtime) {
    return hrtime[0] * 1000 + hrtime[1] * 0.000001; 
  }
  
  static duration(timestamp1, timestamp2) {
    const secondsDiff = timestamp1[0] - timestamp2[0] + ((timestamp1[1] - timestamp2[1]) * 0.000000001);
    const secondsDiffFloor = Math.floor(secondsDiff);
    const milliSecondsDecimal = 1000 * (secondsDiff - secondsDiffFloor);
    return [secondsDiffFloor, milliSecondsDecimal.toFixed(3)]; // TODO: second parameter string ???
  }
  
  static add(timestamp1, timestamp2) {
    const seconds = timestamp1[0] + timestamp2[0];
    const nanoSeconds = timestamp1[1] + timestamp2[1];
    if(nanoSeconds > 999999999 ) {
      if(nanoSeconds < 1999999999) {
        return [seconds + 1, nanoSeconds - 1000000000];
      }
      else {
        const extraSeconds = Math.floor(nanoSeconds * 0.000000001);
        return [seconds + extraSeconds, nanoSeconds - extraSeconds * 1000000000];
      }
    }
    else {
      return [seconds, nanoSeconds];
    }
  }
  
  static sub(timestamp1, timestamp2) {
    const seconds = timestamp1[0] - timestamp2[0];
    const nanoSeconds = timestamp1[1] - timestamp2[1];
    if(nanoSeconds > 999999999 ) {
      if(nanoSeconds < 1999999999) {
        return [seconds + 1, nanoSeconds - 1000000000];
      }
      else {
        const extraSeconds = Math.floor(nanoSeconds * 0.000000001);
        return [seconds + extraSeconds, nanoSeconds - extraSeconds * 1000000000];
      }
    }
    else if(nanoSeconds < 0 ) {
      if(nanoSeconds < -999999999) {
        const extraSeconds = Math.floor(nanoSeconds * 0.000000001);
        return [seconds + extraSeconds, nanoSeconds + extraSeconds * (-1000000000)];
      }
      else {
        return [seconds - 1, nanoSeconds + 1000000000];
      }
    }
    else {
      return [seconds, nanoSeconds];
    }
  }
  
  static greaterThan(timestamp1, timestamp2) {
    if(timestamp1[0] > timestamp2[0]) {
      return true;
    }
    else if (timestamp1[0] === timestamp2[0] && timestamp1[1] > timestamp2[1]) {  
      return true;
    }
    else {
      return false;
    }
  }
}

HighResolutionDate.dateStamp = new Date().getTime();
HighResolutionDate.timeStamp = process.hrtime();

module.exports = HighResolutionTimestamp;
