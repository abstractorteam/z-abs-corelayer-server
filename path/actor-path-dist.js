
'use strict';

const Path = require('path');
const ActorPath = require('./actor-path');


class ActorPathDist {
  constructor() {
    this.actorDistPath = ActorPath.setPath('ActorDistPath', ActorPath.getActorPath(), `${Path.sep}dist`);
    this.actorDistLayersPath = ActorPath.setPath('ActorDistLayersPath', this.actorDistPath, `${Path.sep}Layers`);
    this.actorDistApiPath = ActorPath.setPath('ActorDistApiPath', this.actorDistPath, `${Path.sep}api`);
    this.actorDistActorApiPath = ActorPath.setPath('ActorDistActorApiPath', this.actorDistApiPath, `${Path.sep}actor-api`);
    this.actorDistStackApiPath = ActorPath.setPath('ActorDistStackApiPath', this.actorDistApiPath, `${Path.sep}stack-api`);
    this.actorDistActorsPath = ActorPath.setPath('actorDistActorsPath', this.getActorDistPath(), `${Path.sep}actors`);
    this.actorDistStacksPath = ActorPath.setPath('actorDistStacksPath', this.getActorDistPath(), `${Path.sep}stacks`);
    this.actorDistStacksGlobalPath = ActorPath.setPath('actorDistStacksGlobalPath', this.getActorDistPath(), `${Path.sep}stacks${Path.sep}Stacks-global`);
    this.actorDistStacksLocalPath = ActorPath.setPath('actorDistStacksLocalPath', this.getActorDistPath(), `${Path.sep}stacks${Path.sep}Stacks-local`);
    this.actorDistServerPath = ActorPath.setPath('ActorDistServerPath', this.getActorDistPath(), `${Path.sep}Layers${Path.sep}AppLayer${Path.sep}server`);
    this.actorDistServerPluginDataPath = ActorPath.setPath('ActorDistServerPluginDataPath', this.getActorDistServerPath(), `${Path.sep}plugin-data`);
     
    this.serverActorsGlobalJs = 'serverActorsGlobalJs';
    this.serverActorsLocalJs = 'serverActorsLocalJs';
    this.serverStacksGlobalJs = 'serverStacksGlobalJs';
    this.serverStacksLocalJs = 'serverStacksLocalJs';
  }
  
  getActorDistPath() {
    return this.actorDistPath;
  }
  
  getLayersPath() {
    return this.actorDistLayersPath;
  }
  
  getActorDistActorsPath() {
    return this.actorDistActorsPath;
  }
  
  getActorFile(path) {
    return Path.normalize(`${this.getActorDistActorsPath()}${Path.sep}${path.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}.js`);
  }
  
  getActorCacheName(path) {
    if(path.startsWith('./Actors-global')) {
      return this.serverActorsGlobalJs;
    }
    else if(path.startsWith('./Actors-local')) {
      return this.serverActorsLocalJs;
    }
  }
  
  getActorDistStacksPath() {
    return this.actorDistStacksPath;
  }
  
  getActorDistStacksGlobalPath() {
    return this.actorDistStacksGlobalPath;
  }
  
  getActorDistStacksLocalPath() {
    return this.actorDistStacksLocalPath;
  }
  
  getStackCacheName(path) {
    if(path.startsWith('./Stacks-global')) {
      return this.serverStacksGlobalJs;
    }
    else if(path.startsWith('./Stacks-local')) {
      return this.serverStacksLocalJs;
    }
  }
  
  getActorDistServerPath() {
    return this.actorDistServerPath;
  }
  
  getActorDistServerPluginDataPath() {
    return this.actorDistServerPluginDataPath;
  }
  
  getActorDistServerWorkerLogPath() {
    return this.actorDistServerWorkerLogPath; 
  }
}

module.exports = new ActorPathDist();
