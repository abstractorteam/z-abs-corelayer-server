
'use strict';

const ActorPathCreator = require('../actor-path-creator');
const ActorPathData = require('../actor-path-data');


class ActorDataPaths {
  constructor() {
    this.folderPaths = [
      ActorPathData.getReposFolder(),
      
      ActorPathData.getActorDataGlobalFolder(),
      ActorPathData.getTestAbstractionGlobalFolder(),
      ActorPathData.getActorsGlobalFolder(),
      ActorPathData.getStacksGlobalFolder(),
      ActorPathData.getAddressesGlobalFolder(),
      ActorPathData.getTestDataGlobalFolder(),
      ActorPathData.getSystemUnderTestActorGlobalFolder(),
      ActorPathData.getContentGlobalFolder(),
      ActorPathData.getLoginGlobalFolder(),
      
      ActorPathData.getActorDataLocalFolder(),
      ActorPathData.getDocumentationNoteLocalFilder(),
      ActorPathData.getTestAbstractionLocalFolder(),
      ActorPathData.getActorsLocalFolder(),
      ActorPathData.getStacksLocalFolder(),
      ActorPathData.getAddressesLocalFolder(),
      ActorPathData.getTestDataLocalFolder(),
      ActorPathData.getContentLocalFolder()
    ];
    this.filePaths = [
      {
        path: ActorPathData.getAddressesGlobalNetworksFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalNetworksFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesGlobalInterfacesSutFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalInterfacesSutFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesGlobalInterfacesClientFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalInterfacesClientFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesGlobalInterfacesServerFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalInterfacesServerFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesGlobalAddressesSutFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalAddressesSutFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesGlobalAddressesClientFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalAddressesClientFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesGlobalAddressesServerFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalAddressesServerFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesGlobalSrcFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalSrcFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesGlobalDstFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalDstFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesGlobalSrvFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalSrvFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesGlobalPortsFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalPortsFile(),
        default: []
      },
      {
        path: ActorPathData.getActorsGlobalProject(),
        default: [
          {
            "title": "Actors-global",
            "key": "ad8c5b55-dcf3-435f-a938-13basdf77354___GENERATE_NEW",
            "folder": true,
            "data": {
              "path": ".",
              "types": [
                "actorjs",
                "js"
              ]
            },
            "children": []
          }
        ]
      },
      {
        path: ActorPathData.getActorsLocalProject(),
        default: [
          {
            "title": "Actors-local",
            "key": "ad8c5b55-dcf3-435f-a938-13b515d77354___GENERATE_NEW",
            "folder": true,
            "data": {
              "path": ".",
              "types": [
                "actorjs",
                "js"
              ]
            },
            "children": []
          }
        ]
      },
      {
        path: ActorPathData.getStacksGlobalProject(),
        default: [
          {
            "title": "Stacks-global",
            "key": "dba7e9ca-ae67-4278-9438-0e344fb6b5f7",
            "folder": true,
            "data": {
              "path": ".",
              "types": [
                "actorjs",
                "js"
              ]
            },
            "children": []
          }
        ]
      },
      {
        path: ActorPathData.getStacksLocalProject(),
        default: [
          {
            "title": "Stacks-local",
            "key": "9c0203ea-9434-4639-bf2e-5fb41f3653fa",
            "folder": true,
            "data": {
              "path": ".",
              "types": [
                "actorjs",
                "js"
              ]
            },
            "children": []
          }
        ]
      },
      {
        path: ActorPathData.getTestDataEnvironmentGlobalFile(),
        default: []
      },
      {
        path: ActorPathData.getTestDataEnvironmentLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getTestDataGeneralGlobalFile(),
        default: []
      },
      {
        path: ActorPathData.getTestDataGeneralLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getTestDataLogGlobalFile(),
        default: []
      },
      {
        path: ActorPathData.getLicensesFile(),
        default: []
      },
      {
        path: ActorPathData.getDependenciesFile(),
        default: []
      },
      {
        path: ActorPathData.getTestDataLogLocalFile(),
        default: [/*
          {
            name: 'log-engine',
            value: 'all',
            description: 'none, filter or all'
          },
          {
            name: 'log-debug',
            value: 'all',
            description: 'none, filter or all'
          },
          {
            name: 'log-error',
            value: 'all',
            description: 'none, filter or all'
          },
          {
            name: 'log-warning',
            value: 'all',
            description: 'none, filter or all'
          },
          {
            name: 'log-ip',
            value: 'all',
            description: 'none, filter or all'
          },
          {
            name: 'log-success',
            value: 'all',
            description: 'none, filter or all'
          },
          {
            name: 'log-failure',
            value: 'all',
            description: 'none, filter or all'
          },
          {
            name: 'log-test-data',
            value: 'all',
            description: 'none, filter or all'
          }
        */]
      },
      {
        path: ActorPathData.getTestDataSystemGlobalFile(),
        default: []
      },
      {
        path: ActorPathData.getTestDataSystemLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getSystemUnderTestActorFile(),
        default: {
          name: 'Actor',
          description: 'Default System Under Test. The tool it self acts as the system under test.',
          status: 'static'
        }
      },
      {
        path: ActorPathData.getContentTextLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentTextGlobalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentDocumentsLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentDocumentsGlobalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentImageLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentImageGlobalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentVideoLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentVideoGlobalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentAudioLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentAudioGlobalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentOtherLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentOtherGlobalFile(),
        default: []
      },
      {
        path: ActorPathData.getStacksLocalStyle(),
        default: []
      }
    ];
  }
  
  verifyOrCreate(done) {
    new ActorPathCreator(this.folderPaths, this.filePaths).verifyOrCreate(done);
  }
}

module.exports = ActorDataPaths;
