
'use strict';

const ActorPathCreator = require('../actor-path-creator');
const ActorPath = require('../actor-path');


class ActorPaths {
  constructor() {
    this.folderPaths = [
      ActorPath.getActorTmpPath()
    ];
    this.filePaths = [];
  }
  
  verifyOrCreate(done) {
    new ActorPathCreator(this.folderPaths, this.filePaths).verifyOrCreate(done);
  }
}

module.exports = ActorPaths;
