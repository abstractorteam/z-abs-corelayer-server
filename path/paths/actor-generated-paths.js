
'use strict';

const ActorPathCreator = require('../actor-path-creator');
const ActorPathGenerated = require('../actor-path-generated');
const Path = require('path');


class ActorGeneratedPaths {
  constructor() {
    this.folderPaths = [
      ActorPathGenerated.getActorGeneratedFolder(),
      ActorPathGenerated.getLoginFolder(),
      ActorPathGenerated.getTestCaseFolder(),
      ActorPathGenerated.getTestCaseButtonsFolder(),
      ActorPathGenerated.getTestSuiteFolder(),
      ActorPathGenerated.getTestDebuggerFolder(),
      ActorPathGenerated.getWorkspaceFolder()
    ];
    this.filePaths = [
      {
        path: ActorPathGenerated.getLoginFile(),
        default: {
          labId: 'actor',
          userId: 'actor',
          repo: 'actorjs-data-global',
          systemUnderTest: 'Actor'
        }
      },
      {
        path: ActorPathGenerated.getNetworksFile(),
        default: []
      },
      {
        path: ActorPathGenerated.getAddressesFile(),
        default: {}
      },
      {
        path: ActorPathGenerated.getLoginReport(),
        default: []
      },
      {
        path: ActorPathGenerated.getChosenAddresses(),
        default: {}
      },
      {
        path: ActorPathGenerated.getDependenciesFile(),
        default: {
          time: '',
          dependencies: []
        }
      },
      {
        path: ActorPathGenerated.getTestCasesRecentFile(),
        default: []
      },
      {
        path: ActorPathGenerated.getTestSuitesRecentFile(),
        default: []
      },
      {
        path: ActorPathGenerated.getTestDebuggerFolder(),
        default: {
          breakpoints: []
        }
      },
      {
        path: ActorPathGenerated.getWorkspaceRecentFiles(),
        default: {
          recentWorkspaces: [{
            appName: 'actorjs',
            workspaceName: 'actorjs_new.wrk'
          }]
        }
      }
    ];
  }
  
  verifyOrCreate(done) {
    new ActorPathCreator(this.folderPaths, this.filePaths).verifyOrCreate(done);
  }
}

module.exports = ActorGeneratedPaths;
