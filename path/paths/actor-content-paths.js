
'use strict';

const ActorPathCreator = require('../actor-path-creator');
const ActorPathContent = require('../actor-path-content');


class ActorContentPaths {
  constructor() {
    this.folderPaths = [
      ActorPathContent.getContentFolder(),
      ActorPathContent.getContentLocalFolder(),
      ActorPathContent.getContentGlobalFolder(),
    ];
    this.filePaths = [];
  }
  
  verifyOrCreate(done) {
    new ActorPathCreator(this.folderPaths, this.filePaths).verifyOrCreate(done);
  }
}

module.exports = ActorContentPaths;
