
'use strict';

const Path = require('path');
const ActorPath = require('./actor-path');


class ActorPathContent {
  constructor() {
    this.contentFolder = ActorPath.setPath('ContentFolder', ActorPath.getActorPath(), `..${Path.sep}Content`);
    
    this.contentLocalFolder = ActorPath.setPath('ContentLocalFolder', this.contentFolder, `${Path.sep}Local`);
    
    this.contentGlobalFolder = ActorPath.setPath('ContentGlobalFolder', this.contentFolder, `${Path.sep}actorjs-content-global`);
  }
  
  getContentFolder() {
    return this.contentFolder;
  }
  
  getContentLocalFolder() {
    return this.contentLocalFolder;
  }

  getContentGlobalFolder() {
    return this.contentGlobalFolder;
  }
  
  getContentFile(repoPath, path) {
    return `${repoPath}${Path.sep}${path}`;
  }
}

module.exports = new ActorPathContent();
