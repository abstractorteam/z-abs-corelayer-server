
'use strict';

const Path = require('path');
const ActorPath = require('./actor-path');


class ActorPathProject {
  constructor() {
    this.codeProjectFolder = ActorPath.setPath('CodeProjectFolder', ActorPath.getActorPath(), `${Path.sep}project`);
    this.codeProjectFile = ActorPath.setPath('CodeProjectFile', this.codeProjectFolder, `${Path.sep}actor-project.json`);

    this.clientJsx = 'clientJsx';
    this.clientJs = 'clientJs';
    this.clientServerJs = 'clientServerJs';
    this.serverWorkerLogJs = 'serverWorkerLogJs';
    this.serverWorkerActorJs = 'serverWorkerActorJs';
    this.serverJs = 'serverJs';
  }
  
  getCacheName(path) {
    if(path.startsWith('./project/clientServer')) {
      return this.clientServerJs;
    }
    else if(path.startsWith('./project/client')) {
      if(path.endsWith('jsx')) {
        return this.clientJsx;
      }
      if(path.endsWith('js')) {
        return this.clientJs;
      }
    }
    else if(path.startsWith('./project/server')) {
      return this.serverJs;
    }
    else if(path.startsWith('./project/serverWorkers/serverWorkerLog')) {
      return this.serverWorkerLogJs;
    }
    else if(path.startsWith('./project/serverWorkers/serverWorkerActor')) {
      return this.serverWorkerActorJs;
    }
  }
  
  getActorPathProject() {
    console.warn('depricated. Use getCodeProjectFolder instead.');
    return this.codeProjectFolder;
  }
  
  getCodeProjectFolder() {
    return this.codeProjectFolder;
  }
  
  getCodeProjectFile() {
    return this.codeProjectFile;
  }
  
  getFile(fileName) {
    return Path.normalize(`${ActorPath.getActorPath()}${fileName.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
  }
  
  getFolder(fileName) {
    return Path.normalize(`${ActorPath.getActorPath()}${fileName.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
  }
}

module.exports = new ActorPathProject();
