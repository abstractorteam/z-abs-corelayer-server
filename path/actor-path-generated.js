
'use strict';

const ActorPath = require('./actor-path');
const Path = require('path');


class ActorPathGenerated {
  constructor() {
    this.actor = 'Actor';
    this.actorGeneratedFolder  = ActorPath.setPath('Generated', ActorPath.getActorPath(), `..${Path.sep}Generated`);
    this.actorGeneratedLoginFolder = ActorPath.setPath('Login', this.actorGeneratedFolder, `${Path.sep}Login`);
    this.actorGeneratedTestCaseFolder = ActorPath.setPath('TestCase', this.actorGeneratedFolder, `${Path.sep}TestCase`);
    this.actorGeneratedTestCaseButtonsFolder = ActorPath.setPath('TestCaseButtons', this.actorGeneratedTestCaseFolder, `${Path.sep}TestCaseButtons`);
    this.actorGeneratedTestSuiteFolder = ActorPath.setPath('TestSuite', this.actorGeneratedFolder, `${Path.sep}TestSuite`);
    this.actorGeneratedTestCaseDebuggerFolder = ActorPath.setPath('Debugger', this.actorGeneratedFolder, `${Path.sep}Debugger`);
    this.actorGeneratedWorkspaceFolder = ActorPath.setPath('Workspace', this.actorGeneratedFolder, `${Path.sep}Workspace`);
    
    this.actorGeneratedLoginFile = ActorPath.setPath('login.json', this.actorGeneratedLoginFolder, `${Path.sep}login.json`);
    this.actorGeneratedNetworksFile = ActorPath.setPath('networks.json', this.actorGeneratedLoginFolder, `${Path.sep}networks.json`);
    this.actorGeneratedAddressesFile = ActorPath.setPath('addresses.json', this.actorGeneratedLoginFolder, `${Path.sep}addresses.json`);
    this.actorGeneratedDependenciesFile = ActorPath.setPath('dependencies.json', this.actorGeneratedLoginFolder, `${Path.sep}dependencies.json`);
    /*this.actorGeneratedActorJsDependenciesCacheFile = ActorPath.setPath('actorjs-dependencies-cache.json', this.actorGeneratedLoginFolder, `${Path.sep}actorjs-dependencies-cache.json`);*/
    this.actorGeneratedLoginReport = ActorPath.setPath('login-report.json', this.actorGeneratedLoginFolder, `${Path.sep}login-report.json`);
    this.actorGeneratedChosenAddresses = ActorPath.setPath('chosen-addresses.json', this.actorGeneratedLoginFolder, `${Path.sep}chosen-addresses.json`);
    this.actorGeneratedTestCasesRecentFile = ActorPath.setPath('testCaseRecent.json', this.actorGeneratedTestCaseFolder, `${Path.sep}testCaseRecent.json`);
    this.actorGeneratedTestCaseButtonsFile = ActorPath.setPath('testCaseButtons.json', this.actorGeneratedTestCaseButtonsFolder, `${Path.sep}testCaseButtons`);
    this.actorGeneratedTestSuitesRecentFile = ActorPath.setPath('testSuiteRecent.json', this.actorGeneratedTestSuiteFolder, `${Path.sep}testSuiteRecent.json`);
    this.actorGeneratedWorkspaceFile = ActorPath.setPath('workspace.json', this.actorGeneratedWorkspaceFolder, `${Path.sep}workspace.json`);
  }
  
  getActorGeneratedFolder() {
    return this.actorGeneratedFolder;
  }
  
  getLoginFolder() {
    return this.actorGeneratedLoginFolder;
  }
  
  getLoginFile() {
    return this.actorGeneratedLoginFile;
  }

  getNetworksFile() {
    return this.actorGeneratedNetworksFile;
  }
  
  getAddressesFile() {
    return this.actorGeneratedAddressesFile;
  }
  
  getLoginReport() {
    return this.actorGeneratedLoginReport;
  }
  
  getChosenAddresses() {
    return this.actorGeneratedChosenAddresses;
  }

  getDependenciesFile() {
    return this.actorGeneratedDependenciesFile;
  }
  
  /*getActorJsDependenciesCacheFile() {
    return this.actorGeneratedActorJsDependenciesCacheFile;
  }*/
  
  getTestCaseFolder() {
    return this.actorGeneratedTestCaseFolder;
  }
  
  getTestCaseButtonsFolder() {
    return this.actorGeneratedTestCaseButtonsFolder;
  }

  getTestCasesRecentFile() {
    return this.actorGeneratedTestCasesRecentFile;
  }
  
  getTestCaseButtonsFile(key) {
    return `${this.actorGeneratedTestCaseButtonsFile}_${key}.json`;
  }
  
  getTestCasesButtonsFile() {
    return this.actorGeneratedTestCaseButtonsFile;
  }
  
  getTestSuiteFolder() {
    return this.actorGeneratedTestSuiteFolder;
  }

  getTestSuitesRecentFile() {
    return this.actorGeneratedTestSuitesRecentFile;
  }
  
  getTestDebuggerFolder() {
    return this.actorGeneratedTestCaseDebuggerFolder;
  }
  
  getTestCaseDebuggerFolder(sut, fut) {
    return `${this.actorGeneratedTestCaseDebuggerFolder}${Path.sep}Sut${sut}${Path.sep}Fut${fut}`;
  }
  
  getTestCaseDebuggerFile(sut, fut, tc) {
    return `${this.actorGeneratedTestCaseDebuggerFolder}${Path.sep}Sut${sut}${Path.sep}Fut${fut}${Path.sep}Tc${tc}.json`;
  }
  
  getWorkspaceFolder() {
    return this.actorGeneratedWorkspaceFolder;
  }
  
  getWorkspaceRecentFiles() {
    return this.actorGeneratedWorkspaceFile;
  }
  
  getWorkspaceFile(appName, workspaceName) {
    return `..${Path.sep}${appName}${Path.sep}workspace${Path.sep}${workspaceName}.wrk`;
  }
}

module.exports = new ActorPathGenerated();
