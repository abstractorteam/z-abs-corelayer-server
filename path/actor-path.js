
'use strict';

const Path = require('path');
const Logger = require('../log/logger');


class ActorPath {
  constructor() {
    this.actorPath = this.setPath('ActorFolder', Path.resolve('.'), ``);
    this.actorTmpPath = this.setPath('ActorTmpPath', this.actorPath, `${Path.sep}tmp`);
    
    this.packageJsonFile = this.setPath('PackageJsonFile', this.actorPath, `${Path.sep}package.json`);
    this.packageJsonLockFile = this.setPath('PackageJsonLockFile', this.actorPath, `${Path.sep}package-lock.json`);
  }

  setPath(name, prePath, newPath) {
    const destination = Path.normalize(`${prePath}${Path.sep}${newPath}`);
    LOG_ENGINE(Logger.ENGINE.PTH, `${name}:${ActorPath.space.substring(0, 42 - name.length)}${destination}`);
    return destination;
  }

  getActorPath() {
    return this.actorPath;
  }
  
  getActorTmpPath() {
    return this.actorTmpPath;
  }
  
  getPackageJsonFile() {
    return this.packageJsonFile;
  }
  
  getPackageJsonLockFile() {
    return this.packageJsonLockFile;
  }
}


ActorPath.space = '                                                  ';

module.exports = new ActorPath();
