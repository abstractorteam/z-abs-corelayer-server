
'use strict';

const Path = require('path');
const ActorPath = require('./actor-path');


class ActorPathData {
  constructor() {
    this.actor = 'Actor';
    
    // *** REPO
    this.dataFolder = ActorPath.setPath('DataFolder', ActorPath.getActorPath(), `..${Path.sep}Data`);
    
    // *** LOCAL
    
    this.actorDataLocalFolder = ActorPath.setPath('ActorDataLocalFolder', this.dataFolder, `${Path.sep}Local`);    this.testAbstractionsLocalFolder = ActorPath.setPath('TestAbstractionsLocalFolder', this.actorDataLocalFolder, `${Path.sep}TestAbstractions`);
    this.actorsLocalFolder = ActorPath.setPath('ActorsLocalFolder', this.testAbstractionsLocalFolder, `${Path.sep}Actors-local`);
    this.stacksLocalFolder = ActorPath.setPath('StacksLocalFolder', this.testAbstractionsLocalFolder, `${Path.sep}Stacks-local`);
    this.addressesLocalFolder = ActorPath.setPath('AddressesLocalFolder', this.actorDataLocalFolder, `${Path.sep}Addresses-local`);
    
    this.testDataLocalFolder = ActorPath.setPath('TestDataLocalFolder', this.actorDataLocalFolder, `${Path.sep}TestData`);
    
    this.documentationNoteLocalFolder = ActorPath.setPath('DocumentationNoteLocalFolder', this.actorDataLocalFolder, `${Path.sep}Note`);
    
    this.contentLocalFolder = ActorPath.setPath('ContentLocalFolder', this.actorDataLocalFolder, `${Path.sep}Content`);
    
    // *** GLOBAL
    this.actorDataGlobalFolder  = ActorPath.setPath('ActorDataGlobalFolder', ActorPath.getActorPath(), `..${Path.sep}Data${Path.sep}actorjs-data-global`);
    this.testAbstractionsGlobalFolder = ActorPath.setPath('TestAbstractionsGlobalFolder', this.actorDataGlobalFolder, `${Path.sep}TestAbstractions`);
    this.actorsGlobalFolder = ActorPath.setPath('ActorsGlobalFolder', this.testAbstractionsGlobalFolder, `${Path.sep}Actors-global`);
    this.stacksGlobalFolder = ActorPath.setPath('ActorsGlobalFolder', this.testAbstractionsGlobalFolder, `${Path.sep}Stacks-global`);
    this.addressesGlobalFolder = ActorPath.setPath('AddressesGlobalFolder', this.actorDataGlobalFolder, `${Path.sep}Addresses-global`);
    
    this.testDataGlobalFolder = ActorPath.setPath('TestDataGlobalFolder', this.actorDataGlobalFolder, `${Path.sep}TestData`);
        
    this.contentGlobalFolder = ActorPath.setPath('ContentGlobalFolder', this.actorDataGlobalFolder, `${Path.sep}Content`);
    
    this.loginGlobalFolder = ActorPath.setPath('LoginGlobalFolder', this.actorDataGlobalFolder, `${Path.sep}Login`);
    this.loginGlobalFile = ActorPath.setPath('licenses.json', this.loginGlobalFolder, `${Path.sep}licenses.json`);
    this.dependenciesGlobalFile = ActorPath.setPath('dependencies.json', this.loginGlobalFolder, `${Path.sep}dependencies.json`);
    
    this.testAbstractions = 'TestAbstractions';
  }
  
  getActorDataLocalFolder() {
    return this.actorDataLocalFolder;
  }
  
  getActorDataGlobalFolder() {
    return this.actorDataGlobalFolder;
  }
  
  getReposFolder() {
    return this.dataFolder;
  }
  
  getTestAbstractionGlobalFolder() {
    return this.testAbstractionsGlobalFolder;
  }
  
  getTestAbstractionLocalFolder() {
    return this.testAbstractionsLocalFolder;
  }
  
  getReposRepoFolder(repoName) {
    return `${this.dataFolder}${Path.sep}${repoName}`;
  }
  
  getTestAbstractionFolder(repoName) {
    if('actorjs-data-global' === repoName) {
      return this.getTestAbstractionGlobalFolder();
    }
    else {
      return `${this.dataFolder}${Path.sep}${repoName}${Path.sep}${this.testAbstractions}`;
    }
  }

  getSystemUnderTestFolder(repoName, sutName) {
    if('actorjs-data-global' === repoName) {
      return `${this.testAbstractionsGlobalFolder}${Path.sep}Sut${sutName}`;
    }
    else {
      return `${this.getTestAbstractionFolder(repoName)}${Path.sep}Sut${sutName}`;
    }
  }
  
  getSystemUnderTestFile(repoName, sutName) {
    return `${this.getSystemUnderTestFolder(repoName, sutName)}${Path.sep}Sut${sutName}.json`;
  }
  
  getSystemUnderTestActorGlobalFolder() {
    return `${this.testAbstractionsGlobalFolder}${Path.sep}Sut${this.actor}`;
  }
  
  getSystemUnderTestActorFile() {
    return `${this.getSystemUnderTestFolder('Global', this.actor)}${Path.sep}Sut${this.actor}.json`;
  }
  
  getFunctionUnderTestFolder(repoName, sutName, futName) {
    return `${this.getSystemUnderTestFolder(repoName, sutName)}${Path.sep}Fut${sutName}${futName}`;
  }
  
  getFunctionUnderTestFile(repoName, sutName, futName) {
    return `${this.getFunctionUnderTestFolder(repoName, sutName, futName)}${Path.sep}Fut${sutName}${futName}.json`;
  }
  
  getTestCasesFolder(repoName, sutName, futName) {
    return `${this.getFunctionUnderTestFolder(repoName, sutName, futName)}${Path.sep}TestCases`;
  }
  
  getTestSuitesFolder(repoName, sutName, futName) {
    return `${this.getFunctionUnderTestFolder(repoName, sutName, futName)}${Path.sep}TestSuites`;
  }
  
  getTestCaseFolder(repoName, sutName, futName, tcName) {
    return `${this.getTestCasesFolder(repoName, sutName, futName)}${Path.sep}Tc${tcName}`;
  }
  
  getTestSuiteFolder(repoName, sutName, futName, tsName) {
    return `${this.getTestSuitesFolder(repoName, sutName, futName)}${Path.sep}Ts${tsName}`;
  }

  getTestCaseFile(repoName, sutName, futName, tcName) {
    return `${this.getTestCaseFolder(repoName, sutName, futName, tcName)}${Path.sep}Tc${tcName}.json`;
  }
  
  getTestSuiteFile(repoName, sutName, futName, tsName) {
    return `${this.getTestSuiteFolder(repoName, sutName, futName, tsName)}${Path.sep}Ts${tsName}.json`;
  }
  
  getActorsLocalFolder() {
    return this.actorsLocalFolder;
  }
  
  getActorsGlobalFolder() {
    return this.actorsGlobalFolder;
  }
  
  getActorsLocalProject() {
    return `${this.getActorsLocalFolder()}${Path.sep}actor-actors-local-project.json`;
  }
  
  getActorsGlobalProject() {
    return `${this.getActorsGlobalFolder()}${Path.sep}actor-actors-global-project.json`;
  }

  getActorsProject(path) {
    if('./Actors-global' === path) {
      return this.getActorsGlobalProject();
    }
    else if('./Actors-local' === path ) {
      return this.getActorsLocalProject();
    }
    return 'NotFound';
  }
  
  getActorFile(path) {
    if(path.startsWith('./Actors-global')) {
      return Path.normalize(`${this.getTestAbstractionGlobalFolder()}${Path.sep}${path.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
    }
    else if(path.startsWith('./Actors-local')) {
      return Path.normalize(`${this.getTestAbstractionLocalFolder()}${Path.sep}${path.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
    }
    return 'NotFound';
  }
  
  getStacksLocalFolder() {
    return this.stacksLocalFolder;
  }
  
  getStacksGlobalFolder() {
    return this.stacksGlobalFolder;
  }
  
  getStacksLocalProject() {
    return `${this.getStacksLocalFolder()}${Path.sep}actor-stacks-local-project.json`;
  }
  
  getStacksLocalStyle() {
    return `${this.getStacksLocalFolder()}${Path.sep}stack-style.json`;
  }
  
  getStacksGlobalProject() {
    return `${this.getStacksGlobalFolder()}${Path.sep}actor-stacks-global-project.json`;
  }
  
  getStacksProject(path) {
    if('./Stacks-global' === path) {
      return this.getStacksGlobalProject();
    }
    else if('./Stacks-local' === path ) {
      return this.getStacksLocalProject();
    }
    return 'NotFound';
  }
  
  getStackFile(path) {
    if(path.startsWith('./Stacks-global')) {
      return Path.normalize(`${this.getTestAbstractionGlobalFolder()}${Path.sep}${path.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
    }
    else if(path.startsWith('./Stacks-local')) {
      return Path.normalize(`${this.getTestAbstractionLocalFolder()}${Path.sep}${path.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
    }
    return 'NotFound';
  }
  
  getAddressesLocalFolder() {
    return this.addressesLocalFolder;
  }
  
  getAddressesGlobalFolder() {
    return this.addressesGlobalFolder;
  }
  
  getAddressesLocalNetworksFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}networks.json`;
  }
  
  getAddressesGlobalNetworksFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}networks.json`;
  }

  getAddressesLocalInterfacesSutFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}interfacesSut.json`;
  }
  
  getAddressesGlobalInterfacesSutFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}interfacesSut.json`;
  }

  getAddressesLocalInterfacesClientFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}interfacesClient.json`;
  }
  
  getAddressesGlobalInterfacesClientFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}interfacesClient.json`;
  }

  getAddressesLocalInterfacesServerFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}interfacesServer.json`;
  }
  
  getAddressesGlobalInterfacesServerFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}interfacesServer.json`;
  }
  
  getAddressesLocalAddressesSutFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}addressesSut.json`;
  }

  getAddressesGlobalAddressesSutFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}addressesSut.json`;
  }

  getAddressesLocalAddressesClientFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}addressesClient.json`;
  }

  getAddressesGlobalAddressesClientFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}addressesClient.json`;
  }

  getAddressesLocalAddressesServerFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}addressesServer.json`;
  }

  getAddressesGlobalAddressesServerFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}addressesServer.json`;
  }
  
  getAddressesLocalSrcFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}addressesSrc.json`;
  }

  getAddressesGlobalSrcFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}addressesSrc.json`;
  }

  getAddressesLocalDstFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}addressesDst.json`;
  }

  getAddressesGlobalDstFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}addressesDst.json`;
  }
  
  getAddressesLocalSrvFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}addressesSrv.json`;
  }

  getAddressesGlobalSrvFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}addressesSrv.json`;
  }
  
  getAddressesLocalPortsFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}addressesPorts.json`;
  }

  getAddressesGlobalPortsFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}addressesPorts.json`;
  }

  getTestDataGlobalFolder() {
    return this.testDataGlobalFolder;
  }
  
  getTestDataLocalFolder() {
    return this.testDataLocalFolder;
  }
  
  getTestDataEnvironmentGlobalFile() {
    return `${this.getTestDataGlobalFolder()}${Path.sep}test-data-environment.json`;
  }
  
  getTestDataEnvironmentLocalFile() {
    return `${this.getTestDataLocalFolder()}${Path.sep}test-data-environment.json`;
  }

  getTestDataGeneralGlobalFile() {
    return `${this.getTestDataGlobalFolder()}${Path.sep}test-data-general.json`;
  }
  
  getTestDataGeneralLocalFile() {
    return `${this.getTestDataLocalFolder()}${Path.sep}test-data-general.json`;
  }

  getTestDataLogGlobalFile() {
    return `${this.getTestDataGlobalFolder()}${Path.sep}test-data-log.json`;
  }
  
  getTestDataLogLocalFile() {
    return `${this.getTestDataLocalFolder()}${Path.sep}test-data-log.json`;
  }
  
  getTestDataSystemGlobalFile() {
    return `${this.getTestDataGlobalFolder()}${Path.sep}test-data-system.json`;
  }
  
  getTestDataSystemLocalFile() {
    return `${this.getTestDataLocalFolder()}${Path.sep}test-data-system.json`;
  }
  
  getDocumentationNoteLocalFilder() {
    return this.documentationNoteLocalFolder;
  }
 
  getDocumentationNoteLocalFileAndPath(guid, path) {
    let fullPath = `${this.getDocumentationNoteLocalFilder()}${Path.sep}${path}`;
    let formattedPath = Path.normalize(fullPath.replace(new RegExp('[/\\\\]', 'g'), Path.sep));
    let formattedFile = `${formattedPath}${Path.sep}Local-note-${guid}.txt`;
    return {
      path: formattedPath,
      file: formattedFile
    };
  }
    
  getDocumentationNoteLocalFile(guid, path) {
    return this.getDocumentationNoteLocalFileAndPath(guid, path).file;
  }
  
  getContentLocalFolder() {
    return this.contentLocalFolder;
  }
  
  getContentTextLocalFile() {
    return `${this.contentLocalFolder}${Path.sep}text.json`;
  }
  getContentDocumentsLocalFile() {
    return `${this.contentLocalFolder}${Path.sep}documents.json`;
  }
  
  getContentImageLocalFile() {
    return `${this.contentLocalFolder}${Path.sep}image.json`;
  }
  
  getContentVideoLocalFile() {
    return `${this.contentLocalFolder}${Path.sep}video.json`;
  }
  
  getContentAudioLocalFile() {
    return `${this.contentLocalFolder}${Path.sep}audio.json`;
  }
  
  getContentOtherLocalFile() {
    return `${this.contentLocalFolder}${Path.sep}other.json`;
  }

  getContentGlobalFolder() {
    return this.contentGlobalFolder;
  }
 
  getContentTextGlobalFile() {
    return `${this.contentGlobalFolder}${Path.sep}text.json`;
  }
  
  getContentDocumentsGlobalFile() {
    return `${this.contentGlobalFolder}${Path.sep}documents.json`;
  }
  
  getContentImageGlobalFile() {
    return `${this.contentGlobalFolder}${Path.sep}image.json`;
  }
  
  getContentVideoGlobalFile() {
    return `${this.contentGlobalFolder}${Path.sep}video.json`;
  }
  
  getContentAudioGlobalFile() {
    return `${this.contentGlobalFolder}${Path.sep}audio.json`;
  }
  
  getContentOtherGlobalFile() {
    return `${this.contentGlobalFolder}${Path.sep}other.json`;
  }
  
  getLoginGlobalFolder() {
    return this.loginGlobalFolder;
  }
  
  getLicensesFile() {
    return this.loginGlobalFile;
  }
  
  getDependenciesFile() {
    return this.dependenciesGlobalFile;
  }
}


module.exports = new ActorPathData();
