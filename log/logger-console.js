
'use strict';

let LogConfig = require('./log-config');
let LogTypes = require('./log-types');
let Clc = require('cli-color');


class LoggerConsole {
  constructor() {
    this.id = 0;
  }

  log(logRow) {
    let id = this.id = LoggerConsole.IDS[this.id];
//    setImmediate(() => {
      if(logRow.log.length > LoggerConsole.LOG_SIZE) {
        console.log(LogConfig.config[logRow.type].color[id](`${LogTypes.name[logRow.type]} : ${logRow.date.date.toUTCString()} ${LoggerConsole.MILLI_SECONDS[`${logRow.date.milliSeconds}`.length]}${logRow.date.milliSeconds} : ${LoggerConsole.PID_ZEROS[`${logRow.pid}`.length]}${logRow.pid} : ${logRow.log.substring(0, LoggerConsole.LOG_SIZE - 3)}... : ${LogConfig.config[logRow.type].groups[logRow.group].displayName} : ${logRow.fileName}`));
      }
      else {
        console.log(LogConfig.config[logRow.type].color[id](`${LogTypes.name[logRow.type]} : ${logRow.date.date.toUTCString()} ${LoggerConsole.MILLI_SECONDS[`${logRow.date.milliSeconds}`.length]}${logRow.date.milliSeconds} : ${LoggerConsole.PID_ZEROS[`${logRow.pid}`.length]}${logRow.pid} : ${logRow.log}${LoggerConsole.SPACE_STORE.substring(0, LoggerConsole.LOG_SIZE - logRow.log.length)} : ${LogConfig.config[logRow.type].groups[logRow.group].displayName} : ${logRow.fileName}`));
      }
      if(undefined !== logRow.err) {
        console.log(LogConfig.config[logRow.type].color[id](logRow.err));
      }
//    });
  }
}

LoggerConsole.IDS = [1, 0];
LoggerConsole.SPACE_STORE = '                                                                                                                                                                                                                                                                                                                                              ';
LoggerConsole.LOG_SIZE = 256;
LoggerConsole.MILLI_SECONDS = ['', '', '', '', '', '00', '0', ''];
LoggerConsole.PID_ZEROS = ['', '     ', '    ', '   ', '  ',  ' ', ''];

module.exports = LoggerConsole;
