
'use strict';

const Path = require('path');


class LogWorker {
  constructor() {
    this.worker = null;
    try {
      const Worker = require('webworker-threads').Worker;
  	  this.worker = new Worker(`${Path.resolve('.')}${Path.sep}dist${Path.sep}serverWorkers${Path.sep}serverWorkerLog${Path.sep}server-worker-log-console-bundle.js`);
      this.worker.onMessage = (event) => {
      }
    }
    catch(err) {
      console.log(`WARNING: 'webworker-threads' is not installed the server logs will increase the time of a Test Case with 1000%.`);
    }
  }
  
  isWorking() {
    return null !== this.worker;
  }
  
  log(data) {
    this.worker.postMessage({
      logRow: data,
      logType: 0 // log row
    });
  }

  tcStart(data) {
    this.worker.postMessage({
      logRow: data,
      logType: 1 // tc start
    });
  }
  
  tcStop(data) {
    this.worker.postMessage({
      logRow: data,
      logType: 2 // tc stop
    });
  }
  
  tsStart(data) {
    this.worker.postMessage({
      logRow: data,
      logType: 3 // ts start
    });
  }
  
  tsStop(data) {
    this.worker.postMessage({
      logRow: data,
      logType: 4 // ts stop
    });
  }
}

module.exports = LogWorker;
