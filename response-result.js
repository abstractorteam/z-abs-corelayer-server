
'use strict';


class ResponseResultSuccess {
  constructor() {
    this.code = 'success';
  }
}

class ResponseResultError {
  constructor(msg, error) {
    this.code = 'error';
    if(undefined !== error) {
      this.error = error;
    }
    else {
      this.error = {};
    }
    this.msg = msg;
  }
}

class ResponseResultFailure {
  constructor(msg, status, restType) {
    this.code = 'failure';
    this.msg = msg;
    this.status = status;
    this.restType = restType;
  }
}

module.exports = {
  ResponseResultSuccess,
  ResponseResultError,
  ResponseResultFailure
}
