
'use strict';

const DataResponse = require('./data-response');


class AppActor {
  constructor() {
    this.content = [
      {
        search: '/assets/',
        route: '',
        remove: ''
      },
      {
        search: '/skin-win8/',
        route: '/css',
        remove: '/skin-win8'
      },
      {
        search: '/fonts/',
        route: '',
        remove: ''
      },
      {
        search: '/images/',
        route: '',
        remove: ''
      },
      {
        search: '/actorjs-documentation-bin/',
        route: '',
        remove: ''
      }
    ];
    this.paths = [
      'scripts',
      'css'
    ];
    this.defaultSite = '';
  }
  
  initMainServer(defaultSite, pluginFactoryPaths, wsServers, done) {
    if(defaultSite.startsWith('/')) {
      this.defaultSite  = defaultSite;
    }
    else {
      this.defaultSite  = `/${defaultSite}`;
    }
    DataResponse.initMainServer(pluginFactoryPaths, wsServers, done);
  }
  
  handleHttpRequest(req, res, next) {
    if(req._parsedUrl.pathname.startsWith('/data/')) {
      try {
        if(req.method == 'POST') {
          DataResponse.handleHttpRequest(req, (data) => {
            if(null !== data) {
              this.jsonResponseHttp(res, data);
            }
            else {
              res.writeHead(400, 'Bad Request', {'Content-Length': '0'});
              res.end();
            }
          });
          return;
        }
      }
      catch(err) {
        this.notFound(req, res);
        return;
      }
      this.notImplementedServerError(req, res);
    }
    else if(req._parsedUrl.pathname.startsWith('/scripts/')) {
      next();
    }
    else if(req._parsedUrl.pathname.startsWith('/css/')) {
      next();
    }
    else {
      const foundIndex = this.content.findIndex((content) => (-1 != req._parsedUrl.pathname.indexOf(content.search)));
      if(-1 !== foundIndex) {
        req.url = this.content[foundIndex].route + req.url.substring(req._parsedUrl.pathname.indexOf(this.content[foundIndex].search) + this.content[foundIndex].remove.length);
        next();
      }
      else if(req._parsedUrl.pathname === '/favicon.ico') {
        req.url = '/images/svg/AbstraktorA.svg';
        next();
      }
      else if(req._parsedUrl.pathname === '/') {
        this.redirect(res, this.defaultSite);
      }
      else {
        req.url = "/index.html";
        next();
      }
    }
  }
  
  handleWsConnection(ws) {
    DataResponse.handleWsConnection(ws);
  }
  
  jsonResponseHttp(res, data)
  {
    res.setHeader('Content-type', 'application/json');
    res.write(data);
    res.end();
  }
  
  redirect(res, to) {
    res.writeHead(302, {'Location': to});
    res.end();
  }
  
  notImplementedServerError(req, res) {
    res.writeHead(501, "Not implemented", {'Content-Type': 'text/html'});
    res.end(this.errorHtml('501 - Not implemented', '<h1>Not implemented!</h1>'));
  }
  
  notFound(req, res) {
    res.writeHead(404, 'Not Found', {"Content-Type": "text/plain"});
    res.end(this.errorHtml('404 - Not Found', '<h1>Not Found!</h1>'));
    res.end();
  }
  
  errorHtml(title, bodyText) {
    return '<!DOCTYPE html><html lang="en-US"><head><title>' + title + '</title><meta charset="UTF-8"></head><body>' + bodyText + '</body></html>';
  }
}

module.exports = new AppActor();
