
'use strict';

const ActorPath = require('../../path/actor-path');
const ActorPathProject = require('../../path/actor-path-project');
const ActorPathData = require('../../path/actor-path-data');
const Logger = require('../../log/logger');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');
const Project = require('z-abs-corelayer-cs/project');
const Fs = require('fs');
const Path = require('path');


class DialogFileGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
    this.project = new Project();
    this.pathFilters = [''];
    this.existingFilters = [''];
    this.rootName = '';
  }

  onRequest(path, rootName, pathFilters, existingFilters, type) {
    this.rootName = rootName;
    this.pathFilters = pathFilters;
    this.existingFilters = existingFilters;
    let rootPath = this.getRootPath(this.rootName);
    let node = this.project.addRootFolder(Path.parse(rootPath).base, []);
    this.getFilesTree(rootPath, type, node, (err) => {
      if(!err) {
        this.responsePartSuccess(this.project);
      }
      else {
        this.responsePartError(`Could not get Dialog File: '${rootPath}'`, err);
      }
    });
  }
  
  getFilesTree(dir, type, node, done) {
    Fs.readdir(dir, (err, files) => {
      if(err) {
        return done(err);
      }
      let pendings = files.length;
      if(0 == pendings) {
        done();
        return;
      }
      files.forEach((file) => {
        let newDir = Path.resolve(dir, file);
        let relativeDir = Path.relative(this.getRelativePath(this.rootName), dir).replace(new RegExp('[\\\\]', 'g'), '/');
        Fs.stat(newDir, (err, stat) => {
          if(stat && stat.isDirectory()) {
            let found = false;
            this.pathFilters.forEach((folder) => {
              if(folder === `./${relativeDir}`) {
                let foundFolderInFilter = this.existingFilters.find((fileFromExistingFilter) => {
                  return fileFromExistingFilter === `./${relativeDir}/${file}`;
                });
                if(!foundFolderInFilter && 'folder' === type) {
                  this.project.addFolderToNode(file, `./${relativeDir}`, [], node);
                }
              }
              else if(folder.startsWith(`./${relativeDir}/${file}`)) {
                found = true;
                let newNode = this.project.addFolderToNode(file, `./${relativeDir}`, [], node);
                this.getFilesTree(newDir, type, newNode, (err) => {
                  if(!--pendings) {
                    done();
                  }
                });
              }
            });
            if(!found) {
              if(!--pendings) {
                done();
              }
            }
          }
          else if(stat && stat.isFile()) {
            let foundFolder = this.pathFilters.find((folderFromPathFilter) => {
              return folderFromPathFilter === `./${relativeDir}`;
            });
            if(foundFolder) {
              let foundFileInFilter = this.existingFilters.find((fileFromExistingFilter) => {
                return fileFromExistingFilter === `./${relativeDir}/${file}`;
              });
              if(!foundFileInFilter && 'file' === type) {
                this.project.addFileToNode(file, `./${relativeDir}`, file.substr(file.lastIndexOf('.') + 1, file.length), node);
              }
            }
            if(!--pendings) {
              done();
            }
          }	
        });
      });
    });	
  }
  
  getRootPath(pathName) {
    switch(pathName) {
      case './project':
        return ActorPathProject.getActorPathProject();
      case './Actors-global':
        return ActorPathData.getActorsGlobalFolder();
      case './Actors-local':
        return ActorPathData.getActorsLocalFolder();
      case './Stacks-global':
        return ActorPathData.getStacksGlobalFolder();
      case './Stacks-local':
        return ActorPathData.getStacksLocalFolder();
      default:
        LOG_ERROR(Logger.ERROR.ERR, `getRootPath: '${ActorPathData.getActorsGlobalFolder()}'`, undefined);
        return '';
    }
  }
  
  getRelativePath(pathName) {
    switch(pathName) {
      case './project':
        return DialogFileGet.ACTOR_PROJECT;
      case './Actors-global':
        return DialogFileGet.ACTORS_GLOBAL;
      case './Actors-local':
        return DialogFileGet.ACTORS_LOCAL;
      case './Stacks-global':
        return DialogFileGet.STACKS_GLOBAL;
      case './Stacks-local':
        return DialogFileGet.STACKS_LOCAL;
      default:
      LOG_ERROR(Logger.ERROR.ERR, `getRelativePath: '${pathName}'`, undefined);
        return '';
    }
  }
}

DialogFileGet.ACTOR_PROJECT = Path.normalize(`${ActorPathProject.getActorPathProject()}/..`);
DialogFileGet.ACTORS_GLOBAL = Path.normalize(`${ActorPathData.getActorsGlobalFolder()}/..`);
DialogFileGet.ACTORS_LOCAL = Path.normalize(`${ActorPathData.getActorsLocalFolder()}/..`);
DialogFileGet.STACKS_GLOBAL = Path.normalize(`${ActorPathData.getStacksGlobalFolder()}/..`);
DialogFileGet.STACKS_LOCAL = Path.normalize(`${ActorPathData.getStacksLocalFolder()}/..`);

module.exports = DialogFileGet;
