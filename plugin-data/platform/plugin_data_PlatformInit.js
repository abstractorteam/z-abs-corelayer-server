
'use strict';

const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');
const ActorDataPaths = require('../../path/paths/actor-data-paths');
const ActorGeneratedPaths = require('../../path/paths/actor-generated-paths');


class PlatformInit extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    const index1 = this.expectAsynchResponse();
    new ActorDataPaths().verifyOrCreate((err) => {
      if(!err) {
        return this._asynchXSuccessResponse(index1);
      }
      else {
        return this._asynchXErrorResponse('Could not create core Actors Directories.', err, index1);
      }
    });
    const index2 = this.expectAsynchResponse();
    new ActorGeneratedPaths().verifyOrCreate((err) => {
      if(!err) {
        this._asynchXSuccessResponse(index2);
      }
      else {
        return this._asynchXErrorResponse('Could not create generated Actors Directories.', err, index2);
      }
    });
  }
}

module.exports = PlatformInit;
